package com.example.roelsuntjens.cloudtester.API;

import android.support.v7.internal.view.SupportActionModeWrapper;
import android.telecom.Call;

import com.example.roelsuntjens.cloudtester.model.WiiData;
import com.example.roelsuntjens.cloudtester.model.WiiDataResponse;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.sql.Time;
import java.util.Date;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;
import retrofit.http.HEAD;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by roelsuntjens on 16-09-15.
 */
public class service {

    private static Service service = null;
    private static OkHttpClient client = null;
    private static RestAdapter adapter = null;
    private static String APPLICATIONID = "YGUDFfarP0MFXnoHlAdOee2OcfZ01QI4YgxisyNa";
    private static String RESTAPIKEY = "lrJv1I4Ow2jqgeR5Ovo8HObcMFCbCEVzazhYCBQL";
    private static String TYPE = "application/json";
    private static String API = "https://api.parse.com/1";

    public static Service getInstace() {
        if (service == null) {
            client.interceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request()
                            .newBuilder()
                            .addHeader("X-Parse-Application-Id", APPLICATIONID)
                            .addHeader("X-Parse-REST-API-Key", RESTAPIKEY)
                            .addHeader("Content-Type", TYPE)
                            .build();
                    return chain.proceed(request);
                }
            });
            OkClient okClient = new OkClient(client);
            adapter = new RestAdapter.Builder().setEndpoint(API).setLogLevel(RestAdapter.LogLevel.FULL).setClient(okClient).build();
            service = adapter.create(Service.class);
        }
        return service;
    }

    public static void setClient(OkHttpClient httpClient) {
        client = httpClient;
    }

    public interface Service {

//        @GET("/classes/WiiData")
//        public void getWiiData(Callback<WiiDataResponse> response);

        @GET("/classes/WiiData/{key}")
        public void getWiiData(@Path("key") String key, Callback<WiiData> response);

        @GET("/classes/WiiData/?limit=1&order=-updatedAt")
        public void getWiiData(Callback<WiiDataResponse> response);

        @GET("/classes/WiiData")
        public void list(@Query(value = "order%20by", encodeName = false) String order, Callback<WiiDataResponse> response);

        @GET("/classes/WiiData/?sort=desc")
        public void getHEAD(@Query("updatedAt") Date date, Callback<WiiDataResponse> response);

        @HEAD("/classes/WiiData")
        public void getHEAD(Callback<WiiData> response);

        @GET("/classes/WiiData")
        public void getLastData(Callback<WiiDataResponse> response);

        @GET("/classes/WiiData")
        public void getThousandResults(@Query("limit") int limit, @Query("skip") int skip, Callback<WiiDataResponse> response);

    }

}
