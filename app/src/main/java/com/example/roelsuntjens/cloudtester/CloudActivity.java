package com.example.roelsuntjens.cloudtester;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.roelsuntjens.cloudtester.API.service;
import com.example.roelsuntjens.cloudtester.model.Update;
import com.example.roelsuntjens.cloudtester.model.UpdateBuilder;
import com.example.roelsuntjens.cloudtester.model.WiiData;
import com.example.roelsuntjens.cloudtester.model.WiiDataResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class CloudActivity extends AppCompatActivity {
    private List<EditText> editFields;
    private List<String> feedbackLines;
    private Timer feedbackTimer;
    private TextView feedback;
    private Button cloudButton, startButton, stopButton;
    private Thread startThread, loadingThread;
    private ProgressBar loadingBar;
    private boolean loading = false;
    private int eikaccx = 118;
    private int eikaccy = 119;
    private int eikaccz = 144;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloud);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cloud, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        feedbackLines = new ArrayList<String>();
        initStartThread();
        initButtons();
        initFields();
        initTimer();
    }

    private void initStartThread() {
        this.startThread = new Thread(new Runnable() {
            public void run() {
                try {
                    while (isLoading()) {
                        showLatestWiiData(false);
                        Thread.sleep(100);
                    }
                } catch (InterruptedException e) {

                }
            }
        });
    }

    private void initLoadingThread() {
        this.loadingThread = new Thread(new Runnable() {
            @Override
            public void run() {
                setLoadingBar(true);
                try {
                    while (isLoading()) {
                        Thread.sleep(250);
                    }
                } catch (InterruptedException e) {
                    setLoadingBar(false);
                    setLoading(false);
                }
            }
        });
    }

    private void initButtons() {
        this.cloudButton = (Button) findViewById(R.id.cloudButton);
        this.startButton = (Button) findViewById(R.id.startButton);
        this.stopButton = (Button) findViewById(R.id.stopButton);
        this.cloudButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cloudPressed();
            }
        });
        this.startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPressed();
            }
        });
        this.stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPressed();
            }
        });
    }

    private void initFields() {
        this.feedback = (TextView) findViewById(R.id.feedback_txt);
        this.loadingBar = (ProgressBar) findViewById(R.id.loadingBar);
        this.editFields = new ArrayList<EditText>();
        this.editFields.add((EditText) (findViewById(R.id.button_1_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_2_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_a_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_b_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_min_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_home_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_plus_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_up_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_down_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_left_value)));
        this.editFields.add((EditText) (findViewById(R.id.button_right_value)));
        this.editFields.add((EditText) (findViewById(R.id.posx_value)));
        this.editFields.add((EditText) (findViewById(R.id.posy_value)));
        this.editFields.add((EditText) (findViewById(R.id.accx_value)));
        this.editFields.add((EditText) (findViewById(R.id.accy_value)));
        this.editFields.add((EditText) (findViewById(R.id.accz_value)));
    }

    private void initTimer() {
        feedbackTimer = new Timer("Timer");
        feedbackTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (feedbackLines.size() > 0) {
                    setFeedback(feedbackLines.get(0));
                    feedbackLines.remove(0);
                } else {
                    setFeedback("");
                }
            }
        }, 1000);
    }

    private void cloudPressed() {
        setLoading(true);
        this.startThread.interrupt();
        initLoadingThread();
        this.loadingThread.start();
        showLatestWiiData(true);
    }

    private void startPressed() {
        setLoading(true);
        initStartThread();
        this.startThread.start();
        initLoadingThread();
        this.loadingThread.start();
    }

    private void stopPressed() {
        try {
            this.startThread.interrupt();
            this.loadingThread.interrupt();
        } catch (Exception e) {
            setFeedback("Could not stop!!!! Reason: " + e.getMessage());
        }
    }

    private boolean isLoading() {
        return loading;
    }

    private void setLoading(boolean result) {
        this.loading = result;
    }

    private void setFeedback(final String line) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                feedback.setText(line);
            }
        });
    }

    private void setLoadingBar(final boolean result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingBar.setVisibility(result ? View.VISIBLE : View.INVISIBLE);
            }
        });
    }

    private void showWiiDataByKey(String toUseKey) {
        service.getInstace().getWiiData(toUseKey, new Callback<WiiData>() {
            @Override
            public void success(WiiData wiiDataResponse, Response response) {
                WiiData wiiData = null;
                if (wiiDataResponse != null) {
                    wiiData = wiiDataResponse;
                    wiiData.printAll();
                    Update update = new UpdateBuilder().fill(wiiData).build();
                    showUpdate(update);
                    setFeedback("Data found :)");
                } else {
                    System.out.println("Results is 'null'");
                    setFeedback("No results found");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("Get Wii Data With Key Failed!!\n" + error.getMessage());
                setFeedback("Choosen key could not be found!");
            }
        });
    }

    private void showLatestWiiData(final boolean singleRun) {
        service.getInstace().getWiiData(new Callback<WiiDataResponse>() {
            @Override
            public void success(WiiDataResponse wiiDataResponse, Response response) {
                if (wiiDataResponse.results.size() > 0) {
                    WiiData wii = wiiDataResponse.results.get(0);
                    String feedback1 = wii.accx < eikaccx - 2 && wii.accx != 0 ? "\nBending left" : wii.accx > eikaccx + 2 ? "\nBending right" : "";
                    String feedback2 = wii.accy < eikaccy - 2 && wii.accy != 0 ? "\nBending backward" : wii.accy > eikaccy + 2 ? "\nBending forward" : "";
                    String feedback3 = wii.accz < eikaccz - 46 && wii.accz != 0 ? "\nPlease turn the wiimote uperside down!" : "";
                    setFeedback(feedback1 + feedback2 + feedback3);
                    showUpdate(new UpdateBuilder().fill(wiiDataResponse.results.get(0)).build());
                } else {
                    setFeedback("Wiidata found, size == 0");
                }
                if (singleRun)
                    setLoadingBar(false);
            }

            @Override
            public void failure(RetrofitError error) {
                setFeedback("Failed, reason: " + error.getMessage());
            }
        });
    }

    public void showUpdate(Update update) {
        String text[] = new String[]{
                "" + update.b1,
                "" + update.b2,
                "" + update.ba,
                "" + update.bb,
                "" + update.bmin,
                "" + update.bhome,
                "" + update.bplus,
                "" + update.bup,
                "" + update.bdown,
                "" + update.bleft,
                "" + update.bright,
                "" + update.posx,
                "" + update.posy,
                "" + update.accx,
                "" + update.accy,
                "" + update.accz
        };
        for (int i = 0; i < text.length; i++) {
            editFields.get(i).setText(text[i]);
        }
    }

    public void clearFields(View v) {
        for (EditText field : editFields) {
            field.setText("");
        }
        setFeedback("");
    }
}