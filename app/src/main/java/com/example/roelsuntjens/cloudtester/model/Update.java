package com.example.roelsuntjens.cloudtester.model;

/**
 * Created by roelsuntjens on 16-09-15.
 */

public class Update {
    public int b1, b2, ba, bb, bmin, bhome, bplus, bup, bdown, bleft, bright;
    public int posx, posy, accx, accy, accz;

    public Update(UpdateBuilder b) {
        b1 = b.b1;
        b2 = b.b2;
        ba = b.ba;
        bb = b.bb;
        bmin = b.bmin;
        bplus = b.bplus;
        bhome = b.bhome;
        bup = b.bup;
        bdown = b.bdown;
        bright = b.bright;
        bleft = b.bleft;
        posx = b.posx;
        posy = b.posy;
        accx = b.accx;
        accy = b.accy;
        accz = b.accz;
    }
}