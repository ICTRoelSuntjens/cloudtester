package com.example.roelsuntjens.cloudtester.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by roelsuntjens on 16-09-15.
 */
public class calc {
    public static int compare(String format1, String format2) {
        Date date1 = stringToDate(format1);
        Date date2 = stringToDate(format2);
        return compare(date1, date2);
    }

    public static int compare(Date date1, Date date2) {
//        System.out.println("Date1: " + date1.toString() + " - " + "Date2: " + date2.toString());
        return date1.compareTo(date2);
    }

    public static Date stringToDate(String format) {
        String part1 = format.substring(0, 10);
        String part2 = format.substring(11, format.length()-1);
        format = part1 + " " + part2;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = null;
        try {
            date = sdf.parse(format);
        } catch (ParseException ex) {
            System.out.println("string to date failed! Reason: " + ex.getMessage());
        }
        return date;
    }
}
