package com.example.roelsuntjens.cloudtester.model;

/**
 * Created by roelsuntjens on 16-09-15.
 */
public class WiiData {
    public int buttonA;
    public int buttonB;
    public int button1;
    public int button2;
    public int buttonMin;
    public int buttonHome;
    public int buttonPlus;
    public int buttonUp;
    public int buttonDown;
    public int buttonLeft;
    public int buttonRight;
    public int accx;
    public int accy;
    public int accz;
    public int posx;
    public int posy;
    public String createdAt;
    public String updatedAt;
    public String objectId;


    public void printButtons() {
        print(Acces.BUTTONS);
    }

    public void printPositions() {
        print(Acces.POSITIONS);
    }

    public void printAcceleration() {
        print(Acces.ACCELERATION);
    }

    public void printAll() {
        print(Acces.ALL);
    }

    private void print(Acces acces) {
        System.out.println(getAcces(acces));
    }

    public String getAcces(Acces acces) {
        String line = "";
        switch (acces) {
            case BUTTONS:
                line += getButtonsToString();
                break;
            case POSITIONS:
                line += getPositionToString();
                break;
            case ACCELERATION:
                line += getAccToString();
                break;
            case ALL:
                line += getButtonsToString();
                line += "\n" + getPositionToString();
                line += "\n" + getAccToString();
                break;
            default:
                System.out.println("Unknown Print request");
                return line;
        }
        line += "\n[" + objectId + "]";
        line += "[" + createdAt + "]";
        line += "[" + updatedAt + "]";
        return line;
    }

    private String getButtonsToString() {
        String line = "";
        line += "[" + buttonA + "]";
        line += "[" + buttonB + "]";
        line += "[" + button1 + "]";
        line += "[" + button2 + "]";
        line += "[" + buttonMin + "]";
        line += "[" + buttonHome + "]";
        line += "[" + buttonPlus + "]";
        line += "[" + buttonUp + "]";
        line += "[" + buttonDown + "]";
        line += "[" + buttonLeft + "]";
        line += "[" + buttonRight + "]";
        return line;
    }

    private String getPositionToString() {
        String line = "";
        line += "[" + posx + "]";
        line += "[" + posy + "]";
        return line;
    }

    private String getAccToString() {
        String line = "";
        line += "[" + accx + "]";
        line += "[" + accy + "]";
        line += "[" + accz + "]";
        return line;
    }

    public enum Acces {
        ALL,
        BUTTONS,
        POSITIONS,
        ACCELERATION
    }

}
