package com.example.roelsuntjens.cloudtester;

import android.app.Application;

import com.example.roelsuntjens.cloudtester.API.service;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;

/**
 * Created by roelsuntjens on 16-09-15.
 */
public class CloudTestApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient httpClient = new OkHttpClient();
        try{
            int cacheSize = 10 * 1024 * 1024;
            File cacheDirectory = new File(getCacheDir().getAbsolutePath(), "HttpCache");
            Cache cache = new Cache(cacheDirectory,cacheSize);
            httpClient.setCache(cache);
        }catch(Exception e){
            e.printStackTrace();
        }
        service.setClient(httpClient);
    }
}
