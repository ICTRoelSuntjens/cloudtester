package com.example.roelsuntjens.cloudtester.model;

/**
 * Created by roelsuntjens on 16-09-15.
 */
public class UpdateBuilder {
    public int b1, b2, ba, bb, bmin, bhome, bplus, bup, bdown, bleft, bright;
    public int posx, posy, accx, accy, accz;

    public UpdateBuilder() {
        b1 = 0;
        b2 = 0;
        ba = 0;
        bb = 0;
        bmin = 0;
        bhome = 0;
        bplus = 0;
        bup = 0;
        bdown = 0;
        bleft = 0;
        bright = 0;
        posx = 0;
        posy = 0;
        accx = 0;
        accy = 0;
        accz = 0;
    }

    public UpdateBuilder setB1() {
        b1 = 1;
        return this;
    }

    public UpdateBuilder setB2() {
        b2 = 1;
        return this;
    }

    public UpdateBuilder setBA() {
        ba = 1;
        return this;
    }

    public UpdateBuilder setBB() {
        bb = 1;
        return this;
    }

    public UpdateBuilder setBMin() {
        bmin = 1;
        return this;
    }

    public UpdateBuilder setBHome() {
        bhome = 1;
        return this;
    }

    public UpdateBuilder setBPlus() {
        bplus = 1;
        return this;
    }

    public UpdateBuilder setBUp() {
        bup = 1;
        return this;
    }

    public UpdateBuilder setBDown() {
        bdown = 1;
        return this;
    }

    public UpdateBuilder setBLeft() {
        bleft = 1;
        return this;
    }

    public UpdateBuilder setBRight() {
        bright = 1;
        return this;
    }

    public UpdateBuilder setPosX(int x) {
        posx = x;
        return this;
    }

    public UpdateBuilder setPosY(int y) {
        posy = y;
        return this;
    }

    public UpdateBuilder setAccX(int x) {
        accx = x;
        return this;
    }

    public UpdateBuilder setAccY(int y) {
        accx = y;
        return this;
    }

    public UpdateBuilder setAccZ(int z) {
        accx = z;
        return this;
    }

    public UpdateBuilder fill(WiiData data) {
        b1 = data.button1;
        b2 = data.button2;
        ba = data.buttonA;
        bb = data.buttonB;
        bmin = data.buttonMin;
        bhome = data.buttonHome;
        bplus = data.buttonPlus;
        bup = data.buttonUp;
        bdown = data.buttonDown;
        bleft = data.buttonLeft;
        bright = data.buttonRight;
        posx = data.posx;
        posy = data.posy;
        accx = data.accx;
        accy = data.accy;
        accz = data.accz;
        return this;
    }

    public Update build() {
        return new Update(this);
    }
}

